package packet

import (
	"fmt"
)

type Packet struct {
	SourceAddress Address
	Payload       Payload
}

type Payload struct {
	Action any
	Data   []byte
}

func (p Packet) IsEmpty() bool {
	if p.Payload.Action == "" {
		return true
	}

	if p.SourceAddress.String() == "" {
		return true
	}

	return false
}

type Address struct {
	Host string
	Port int
}

func (a Address) String() string {
	if a.Port == 0 {
		if a.Host == "" {
			return ""
		}

		return a.Host
	}

	return fmt.Sprintf("%s:%d", a.Host, a.Port)
}
