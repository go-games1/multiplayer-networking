package packet_test

import (
	"fmt"
	"log"
	"sync"

	"gitlab.com/go-games1/multiplayer-networking/transport/packet"
	"gitlab.com/go-games1/multiplayer-networking/transport/udp"
)

func ExampleChannel() {
	var wg sync.WaitGroup

	clientTx, err := udp.NewTransceiver(nil, 4009)
	if err != nil {
		log.Fatal(err)
	}

	serverTx, err := udp.NewTransceiver(nil, 4010)

	clientChannel := packet.NewChannel(clientTx)
	serverChannel := packet.NewChannel(serverTx)

	go func() {
		wg.Add(1)

		p, err := clientChannel.Listen()
		if err != nil {
			log.Fatal(err)
		}

		fmt.Printf("%s: %s\n", p.SourceAddress.String(), p.Payload.Action)

		wg.Done()
	}()

	go func() {
		wg.Add(1)

		p, err := serverChannel.Listen()
		if err != nil {
			log.Fatal(err)
		}

		fmt.Printf("Received %s from %s\n", p.Payload.Action, p.SourceAddress.String())

		if err := serverChannel.Send(p.Payload, p.SourceAddress); err != nil {
			log.Fatal(err)
		}

		wg.Done()
	}()

	if err := clientChannel.Send(
		packet.Payload{
			Action: "HELLO_MSG",
		},
		packet.Address{
			Host: "127.0.0.1",
			Port: 4010,
		},
	); err != nil {
		log.Fatal(err)
	}

	wg.Wait()

	// Output:
	// Received HELLO_MSG from 127.0.0.1:4009
	// 127.0.0.1:4010: HELLO_MSG
}
