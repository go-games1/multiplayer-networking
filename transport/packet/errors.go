package packet

import "github.com/juju/errors"

var ErrNoSourceAddress = errors.New("packet: no source address")
