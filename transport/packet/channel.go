package packet

import (
	"bytes"
	"encoding/gob"
	"net"
	"strconv"
	"strings"

	"github.com/juju/errors"
)

type Transceiver interface {
	Listen() ([]byte, net.Addr, error)
	Send(payload []byte, host string, port int) error
}

type Channel struct {
	Transceiver Transceiver
}

func NewChannel(transceiver Transceiver) *Channel {
	return &Channel{
		Transceiver: transceiver,
	}
}

func (d *Channel) Listen() (Packet, error) {
	data, clientAddr, err := d.Transceiver.Listen()
	if err != nil {
		return Packet{}, errors.Trace(err)
	}

	if len(data) == 0 {
		return Packet{}, nil
	}

	payload, err := d.decodeData(data)
	if err != nil {
		return Packet{}, errors.Trace(err)
	}

	sourceAddress, err := toAddress(clientAddr)
	if err != nil {
		return Packet{}, errors.Trace(err)

	}

	return Packet{
		SourceAddress: sourceAddress,
		Payload:       payload,
	}, errors.Trace(err)
}

func (d *Channel) Send(payload Payload, destination Address) error {
	var buf bytes.Buffer

	enc := gob.NewEncoder(&buf)
	if err := enc.Encode(payload); err != nil {
		return errors.Trace(err)
	}

	return errors.Trace(d.Transceiver.Send(buf.Bytes(), destination.Host, destination.Port))
}

func (d *Channel) decodeData(data []byte) (Payload, error) {
	buf := bytes.NewBuffer(data)
	dec := gob.NewDecoder(buf)

	payload := Payload{}

	if err := dec.Decode(&payload); err != nil {
		return Payload{}, errors.Trace(err)
	}

	return payload, nil
}

func toAddress(addr net.Addr) (Address, error) {
	address := strings.Split(addr.String(), ":")
	if len(address) == 0 {
		return Address{}, ErrNoSourceAddress
	}

	var (
		host = address[0]
		port = 0
	)

	if len(address) == 2 {
		var err error
		if port, err = strconv.Atoi(address[1]); err != nil {
			return Address{}, errors.Trace(err)
		}
	}

	return Address{
		Host: host,
		Port: port,
	}, nil
}
