package session

import (
	"encoding/base64"
	"io"

	"github.com/juju/errors"
)

type challengeBuilder struct {
	length int
	reader io.Reader
}

func (b *challengeBuilder) Build() (string, error) {
	bytes, err := b.generateRandomBytes(b.length)
	return base64.URLEncoding.EncodeToString(bytes), errors.Trace(err)
}

func (b *challengeBuilder) generateRandomBytes(n int) ([]byte, error) {
	bytes := make([]byte, n)
	_, err := b.reader.Read(bytes)
	if err != nil {
		return nil, errors.Trace(err)
	}

	return bytes, nil
}
