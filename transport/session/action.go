package session

var (
	Request  Action = "SESSION_REQ"
	Accepted Action = "SESSION_ACCEPTED"

	ChallengeRequest  Action = "CHALLENGE_REQ"
	ChallengeResponse Action = "CHALLENGE_RESP"

	DisconnectRequest Action = "DISCONNECT_REQ"

	Tick Action = "TICK"

	Unknown Action = "UNKNOWN"
)

type Action string
