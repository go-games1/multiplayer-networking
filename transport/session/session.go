package session

import (
	"time"

	"gitlab.com/go-games1/multiplayer-networking/transport/packet"
)

type Session struct {
	address packet.Address

	state      State
	lastUpdate time.Time

	challenge string
}

func isSessionInactive(s Session, inactivityDuration time.Duration) bool {
	return time.Since(s.lastUpdate) > inactivityDuration
}

type State string

const Disconnected State = "DISCONNECTED"
const Connecting State = "CONNECTING"
const Connected State = "CONNECTED"
