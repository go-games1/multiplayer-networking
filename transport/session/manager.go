package session

import (
	"crypto/rand"
	"log"
	"sync"
	"time"

	"github.com/juju/errors"
	"gitlab.com/go-games1/multiplayer-networking/transport/packet"
)

type PacketChannel interface {
	Listen() (packet.Packet, error)
	Send(payload packet.Payload, destination packet.Address) error
}

type Manager interface {
	Manage(action Action, addr packet.Address, payload packet.Payload) error
	Connect(address packet.Address, challenge string) error
	Disconnect(address packet.Address)
}

type InMemoryManager struct {
	logger *log.Logger

	packetChannel PacketChannel

	maxSessions      int
	numActiveSession int
	activeSessions   map[packet.Address]Session
	challengeBuilder challengeBuilder

	ttl time.Duration

	mu sync.Mutex
}

func NewInMemoryManager(
	logger *log.Logger,
	packetChannel PacketChannel,
	maxConnections int,
	inactivityDuration time.Duration,
) *InMemoryManager {
	handler := &InMemoryManager{
		logger: logger,

		packetChannel: packetChannel,

		maxSessions:      maxConnections,
		numActiveSession: 0,
		activeSessions:   map[packet.Address]Session{},
		challengeBuilder: challengeBuilder{
			length: 30,
			reader: rand.Reader,
		},

		ttl: inactivityDuration,
	}

	go func() {
		for {
			time.Sleep(handler.ttl / 4)
			handler.cleanUp()
		}
	}()

	return handler
}

func (h *InMemoryManager) Manage(action Action, addr packet.Address, payload packet.Payload) error {
	switch action {
	case Request:
		return errors.Trace(h.Challenge(addr))
	case ChallengeResponse:
		return errors.Trace(h.Connect(addr, string(payload.Data)))
	case DisconnectRequest:
		h.Disconnect(addr)
		return nil
	}

	h.Refresh(addr)
	return nil
}

func (h *InMemoryManager) Connect(address packet.Address, challenge string) error {
	h.mu.Lock()
	defer h.mu.Unlock()

	s, ok := h.activeSessions[address]
	if !ok || s.state == Disconnected {
		return ErrChallengeNotRequested
	}

	if s.state == Connected {
		return nil
	}

	if h.numActiveSession >= h.maxSessions {
		delete(h.activeSessions, address)

		return ErrServerFull
	}

	if s.state == Connecting {
		if s.challenge != challenge {
			return ErrWrongChallenge
		}

		h.activeSessions[address] = Session{
			address:    address,
			state:      Connected,
			lastUpdate: now(),
		}
		h.numActiveSession++

		h.logger.Printf("%s connected\n", address.String())
	}

	return nil
}

func (h *InMemoryManager) Challenge(address packet.Address) error {
	h.mu.Lock()
	defer h.mu.Unlock()

	s, ok := h.activeSessions[address]
	if !ok || s.state == Disconnected {
		challenge, err := h.challengeBuilder.Build()
		if err != nil {
			return errors.Trace(err)
		}

		h.activeSessions[address] = Session{
			address:    address,
			state:      Connecting,
			lastUpdate: now(),
			challenge:  challenge,
		}

		if err := h.packetChannel.Send(
			packet.Payload{
				Action: ChallengeRequest,
				Data:   []byte(challenge),
			},
			address,
		); err != nil {
			return errors.Trace(err)
		}
	}

	return nil
}

func (h *InMemoryManager) Refresh(address packet.Address) {
	h.mu.Lock()
	defer h.mu.Unlock()

	if _, ok := h.activeSessions[address]; !ok {
		return
	}

	h.activeSessions[address] = Session{
		address:    address,
		state:      Connected,
		lastUpdate: now(),
	}
}

func (h *InMemoryManager) Disconnect(address packet.Address) {
	h.mu.Lock()
	s, ok := h.activeSessions[address]
	var fromState State
	if ok {
		fromState = s.state
		delete(h.activeSessions, address)
		if fromState == Connected {
			h.numActiveSession--
		}
	}
	h.mu.Unlock()

	h.logger.Printf("%s from %s to %s\n", address.String(), fromState, Disconnected)
	h.logger.Printf("connected sessions: %d", h.numActiveSession)
}

func (h *InMemoryManager) cleanUp() {
	for address, session := range h.activeSessions {
		if time.Since(session.lastUpdate) > h.ttl {
			h.Disconnect(address)

			continue
		}

		if session.state == Disconnected {
			h.Disconnect(address)
		}
	}

	if h.numActiveSession < 0 {
		h.numActiveSession = 0
	}
}

var now = time.Now
