package session

import "github.com/juju/errors"

var ErrServerFull = errors.New("session: server reached max sessions")
var ErrChallengeNotRequested = errors.New("session: challenge not requested yet")
var ErrWrongChallenge = errors.New("session: wrong challenge")
