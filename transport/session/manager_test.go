package session

import (
	"bytes"
	"log"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/go-games1/multiplayer-networking/transport/packet"
)

var logger = log.New(&bytes.Buffer{}, "", 0)

func TestNewInMemoryManager_Challenge(t *testing.T) {
	channel := &mockPacketChannel{}
	challengeBuilder := challengeBuilder{
		length: 1,
		reader: &mockReader{length: 1},
	}
	date := time.Date(2020, 01, 01, 01, 01, 01, 01, time.UTC)
	now = func() time.Time { return date }
	firstAddress := packet.Address{
		Host: "127.0.0.1",
		Port: 1234,
	}

	t.Run("success", func(t *testing.T) {
		manager := NewInMemoryManager(logger, channel, 1, time.Minute)
		manager.challengeBuilder = challengeBuilder

		assert.Len(t, manager.activeSessions, 0)
		assert.Equal(t, manager.numActiveSession, 0)

		challenge, err := challengeBuilder.Build()
		assert.Nil(t, err)

		err = manager.Challenge(firstAddress)
		assert.Nil(t, err)
		assert.Equal(t, manager.activeSessions[firstAddress], Session{
			address:    firstAddress,
			state:      Connecting,
			lastUpdate: date,
			challenge:  challenge,
		})

		assert.Len(t, manager.activeSessions, 1)
		assert.Equal(t, manager.numActiveSession, 0)
	})

	t.Run("challenge_twice", func(t *testing.T) {
		manager := NewInMemoryManager(logger, channel, 1, time.Minute)
		manager.challengeBuilder = challengeBuilder

		challenge, err := challengeBuilder.Build()
		assert.Nil(t, err)

		manager.activeSessions[firstAddress] = Session{
			address:    firstAddress,
			state:      Connecting,
			lastUpdate: date,
			challenge:  challenge,
		}

		assert.Len(t, manager.activeSessions, 1)
		assert.Equal(t, manager.numActiveSession, 0)

		err = manager.Challenge(firstAddress)
		assert.Nil(t, err)
		assert.Equal(t, manager.activeSessions[firstAddress], Session{
			address:    firstAddress,
			state:      Connecting,
			lastUpdate: date,
			challenge:  challenge,
		})
	})

	t.Run("challenge_connected", func(t *testing.T) {
		manager := NewInMemoryManager(logger, channel, 1, time.Minute)
		manager.challengeBuilder = challengeBuilder

		manager.activeSessions[firstAddress] = Session{
			address:    firstAddress,
			state:      Connected,
			lastUpdate: date,
			challenge:  "",
		}
		manager.numActiveSession++

		assert.Len(t, manager.activeSessions, 1)
		assert.Equal(t, manager.numActiveSession, 1)

		err := manager.Challenge(firstAddress)
		assert.Nil(t, err)
		assert.Equal(t, manager.activeSessions[firstAddress], Session{
			address:    firstAddress,
			state:      Connected,
			lastUpdate: date,
			challenge:  "",
		})
	})

	t.Run("challenge_disconnected", func(t *testing.T) {
		manager := NewInMemoryManager(logger, channel, 1, time.Minute)
		manager.challengeBuilder = challengeBuilder

		challenge, err := challengeBuilder.Build()
		assert.Nil(t, err)

		manager.activeSessions[firstAddress] = Session{
			address:    firstAddress,
			state:      Disconnected,
			lastUpdate: date,
			challenge:  "",
		}

		assert.Len(t, manager.activeSessions, 1)
		assert.Equal(t, manager.numActiveSession, 0)

		err = manager.Challenge(firstAddress)
		assert.Nil(t, err)
		assert.Equal(t, manager.activeSessions[firstAddress], Session{
			address:    firstAddress,
			state:      Connecting,
			lastUpdate: date,
			challenge:  challenge,
		})

		assert.Len(t, manager.activeSessions, 1)
		assert.Equal(t, manager.numActiveSession, 0)
	})
}

func TestInMemoryManager_Connect(t *testing.T) {
	channel := &mockPacketChannel{}
	challengeBuilder := challengeBuilder{
		length: 1,
		reader: &mockReader{length: 1},
	}
	date := time.Date(2020, 01, 01, 01, 01, 01, 01, time.UTC)
	now = func() time.Time { return date }
	firstAddress := packet.Address{
		Host: "127.0.0.1",
		Port: 1234,
	}

	t.Run("success", func(t *testing.T) {
		manager := NewInMemoryManager(logger, channel, 1, time.Minute)
		manager.challengeBuilder = challengeBuilder

		assert.Len(t, manager.activeSessions, 0)
		assert.Equal(t, manager.numActiveSession, 0)

		err := manager.Challenge(firstAddress)
		assert.Nil(t, err)

		challenge, err := challengeBuilder.Build()
		assert.Nil(t, err)

		err = manager.Connect(firstAddress, challenge)
		assert.Nil(t, err)
		assert.Equal(t, manager.activeSessions[firstAddress], Session{
			address:    firstAddress,
			state:      Connected,
			lastUpdate: date,
		})
		assert.Len(t, manager.activeSessions, 1)
		assert.Equal(t, manager.numActiveSession, 1)
	})

	t.Run("success_disconnected", func(t *testing.T) {
		manager := NewInMemoryManager(logger, channel, 1, time.Minute)
		manager.challengeBuilder = challengeBuilder

		manager.activeSessions[firstAddress] = Session{
			address:    firstAddress,
			state:      Disconnected,
			lastUpdate: date,
		}

		assert.Len(t, manager.activeSessions, 1)
		assert.Equal(t, manager.numActiveSession, 0)

		err := manager.Challenge(firstAddress)
		assert.Nil(t, err)

		challenge, err := challengeBuilder.Build()
		assert.Nil(t, err)

		err = manager.Connect(firstAddress, challenge)
		assert.Nil(t, err)
		assert.Equal(t, manager.activeSessions[firstAddress], Session{
			address:    firstAddress,
			state:      Connected,
			lastUpdate: date,
		})
		assert.Len(t, manager.activeSessions, 1)
		assert.Equal(t, manager.numActiveSession, 1)
	})

	t.Run("disconnected_challenge_not_requested", func(t *testing.T) {
		manager := NewInMemoryManager(logger, channel, 1, time.Minute)
		manager.challengeBuilder = challengeBuilder

		manager.activeSessions[firstAddress] = Session{
			address:    firstAddress,
			state:      Disconnected,
			lastUpdate: date,
		}

		assert.Len(t, manager.activeSessions, 1)
		assert.Equal(t, manager.numActiveSession, 0)

		err := manager.Connect(firstAddress, "")
		assert.ErrorIs(t, err, ErrChallengeNotRequested)
		assert.Len(t, manager.activeSessions, 1)
		assert.Equal(t, manager.numActiveSession, 0)
	})

	t.Run("challenge_not_requested", func(t *testing.T) {
		manager := NewInMemoryManager(logger, channel, 1, time.Minute)
		manager.challengeBuilder = challengeBuilder

		assert.Len(t, manager.activeSessions, 0)
		assert.Equal(t, manager.numActiveSession, 0)

		err := manager.Connect(firstAddress, "")
		assert.ErrorIs(t, err, ErrChallengeNotRequested)
		assert.Len(t, manager.activeSessions, 0)
		assert.Equal(t, manager.numActiveSession, 0)
	})

	t.Run("server_is_full", func(t *testing.T) {
		manager := NewInMemoryManager(logger, channel, 0, time.Minute)
		manager.challengeBuilder = challengeBuilder

		assert.Len(t, manager.activeSessions, 0)
		assert.Equal(t, manager.numActiveSession, 0)

		err := manager.Challenge(firstAddress)
		assert.Nil(t, err)

		challenge, err := challengeBuilder.Build()
		assert.Nil(t, err)

		err = manager.Connect(firstAddress, challenge)
		assert.ErrorIs(t, err, ErrServerFull)
	})

	t.Run("already_connected", func(t *testing.T) {
		manager := NewInMemoryManager(logger, channel, 1, time.Minute)
		manager.challengeBuilder = challengeBuilder

		manager.activeSessions[firstAddress] = Session{
			address:    firstAddress,
			state:      Connected,
			lastUpdate: date,
		}
		manager.numActiveSession++

		assert.Len(t, manager.activeSessions, 1)
		assert.Equal(t, manager.numActiveSession, 1)

		err := manager.Connect(firstAddress, "")
		assert.Nil(t, err)
		assert.Len(t, manager.activeSessions, 1)
		assert.Equal(t, manager.numActiveSession, 1)
	})
}

func TestInMemoryManager_Refresh(t *testing.T) {
	channel := &mockPacketChannel{}
	date := time.Date(2020, 01, 01, 01, 01, 01, 01, time.UTC)
	now = func() time.Time { return date }
	firstAddress := packet.Address{
		Host: "127.0.0.1",
		Port: 1234,
	}

	t.Run("success", func(t *testing.T) {
		manager := NewInMemoryManager(logger, channel, 1, time.Minute)

		manager.activeSessions[firstAddress] = Session{
			address:    firstAddress,
			state:      Connected,
			lastUpdate: date,
		}

		newDate := date.Add(time.Hour * 24 * 366)
		now = func() time.Time { return newDate }

		manager.Refresh(firstAddress)
		assert.Equal(t, map[packet.Address]Session{
			firstAddress: {
				address:    firstAddress,
				state:      Connected,
				lastUpdate: newDate,
			},
		}, manager.activeSessions)
	})

	t.Run("do_nothing", func(t *testing.T) {
		manager := NewInMemoryManager(logger, channel, 1, time.Minute)

		manager.Refresh(firstAddress)
		assert.Empty(t, manager.activeSessions)
	})
}

func TestInMemoryManager_Disconnect(t *testing.T) {
	channel := &mockPacketChannel{}
	date := time.Date(2020, 01, 01, 01, 01, 01, 01, time.UTC)
	now = func() time.Time { return date }
	firstAddress := packet.Address{
		Host: "127.0.0.1",
		Port: 1234,
	}

	t.Run("disconnect_connected", func(t *testing.T) {
		manager := NewInMemoryManager(logger, channel, 1, time.Minute)
		manager.activeSessions[firstAddress] = Session{
			address:    firstAddress,
			state:      Connected,
			lastUpdate: date.Add(-time.Hour * 24 * 365),
		}
		manager.numActiveSession++

		manager.Disconnect(firstAddress)
		assert.Len(t, manager.activeSessions, 0)
		assert.Equal(t, 0, manager.numActiveSession)
	})

	t.Run("disconnect_connecting", func(t *testing.T) {
		manager := NewInMemoryManager(logger, channel, 1, time.Minute)
		manager.activeSessions[firstAddress] = Session{
			address:    firstAddress,
			state:      Connecting,
			lastUpdate: date.Add(-time.Hour * 24 * 365),
		}

		manager.Disconnect(firstAddress)
		assert.Len(t, manager.activeSessions, 0)
		assert.Equal(t, 0, manager.numActiveSession)
	})

	t.Run("disconnect_disconnected", func(t *testing.T) {
		manager := NewInMemoryManager(logger, channel, 1, time.Minute)
		manager.activeSessions[firstAddress] = Session{
			address:    firstAddress,
			state:      Disconnected,
			lastUpdate: date.Add(-time.Hour * 24 * 365),
		}

		manager.Disconnect(firstAddress)
		assert.Len(t, manager.activeSessions, 0)
		assert.Equal(t, 0, manager.numActiveSession)
	})

	t.Run("session_doesnt_exist", func(t *testing.T) {
		manager := NewInMemoryManager(logger, channel, 1, time.Minute)

		assert.Len(t, manager.activeSessions, 0)
		assert.Equal(t, 0, manager.numActiveSession)

		manager.Disconnect(firstAddress)

		assert.Len(t, manager.activeSessions, 0)
		assert.Equal(t, 0, manager.numActiveSession)
	})
}

/*
func TestInMemoryManager(t *testing.T) {
	channel := &mockPacketChannel{}
	t.Run("success", func(t *testing.T) {
		manager := NewInMemoryManager(logger, channel, 1, time.Minute)
		firstAddress := packet.Address{
			Host: "127.0.0.1",
			Port: 1234,
		}
		secondAddress := packet.Address{
			Host: "127.0.0.1",
			Port: 1236,
		}

		err := manager.Connect(firstAddress)
		assert.Nil(t, err)
		assert.Len(t, manager.activeSessions, 1)

		err = manager.Connect(firstAddress)
		assert.Nil(t, err)
		assert.Len(t, manager.activeSessions, 1)

		err = manager.Connect(secondAddress)
		assert.ErrorIs(t, err, ErrServerFull)
		assert.Len(t, manager.activeSessions, 1)

		manager.Disconnect(firstAddress)
		assert.Len(t, manager.activeSessions, 0)
	})

	t.Run("connect_disconnected", func(t *testing.T) {
		manager := NewInMemoryManager(logger, channel, 5, time.Minute)
		firstAddress := packet.Address{
			Host: "127.0.0.1",
			Port: 1234,
		}

		manager.activeSessions[firstAddress] = Session{
			address:    firstAddress,
			state:      Disconnected,
			lastUpdate: time.Date(2020, 01, 01, 01, 01, 01, 01, time.UTC),
		}
		assert.Equal(t, 0, manager.numActiveSession)
		assert.Len(t, manager.activeSessions, 1)

		err := manager.Connect(firstAddress)
		assert.Nil(t, err)
		assert.Equal(t, 1, manager.numActiveSession)
		assert.Len(t, manager.activeSessions, 1)
	})
}

func TestInMemoryManager_CleanUp(t *testing.T) {
	manager := InMemoryManager{
		logger:           logger,
		maxSessions:      2,
		numActiveSession: 0,
		activeSessions:   map[packet.Address]Session{},
		ttl:              time.Microsecond,
		mu:               sync.Mutex{},
	}

	firstAddress := packet.Address{
		Host: "127.0.0.1",
		Port: 1234,
	}
	secondAddress := packet.Address{
		Host: "127.0.0.1",
		Port: 555,
	}

	_ = manager.Connect(firstAddress)
	_ = manager.Connect(secondAddress)
	assert.Len(t, manager.activeSessions, 2)
	assert.Equal(t, 2, manager.numActiveSession)

	manager.activeSessions[firstAddress] = Session{
		address:    firstAddress,
		state:      Connected,
		lastUpdate: time.Time{},
	}
	manager.activeSessions[secondAddress] = Session{
		address:    secondAddress,
		state:      Disconnected,
		lastUpdate: time.Time{},
	}
	assert.Len(t, manager.activeSessions, 2)

	manager.cleanUp()

	assert.Len(t, manager.activeSessions, 0)
}

func TestInMemoryManager_Manage(t *testing.T) {
	channel := &mockPacketChannel{}
	manager := NewInMemoryManager(logger, channel, 1, time.Minute*60)
	firstAddress := packet.Address{
		Host: "127.0.0.1",
		Port: 1234,
	}
	secondAddress := packet.Address{
		Host: "127.0.0.1",
		Port: 1236,
	}

	err := manager.Manage("rAnDoM", firstAddress, packet.Payload{})
	assert.Nil(t, err)
	assert.Len(t, manager.activeSessions, 0)

	err = manager.Manage(Request, firstAddress, packet.Payload{})
	assert.Nil(t, err)
	assert.Len(t, manager.activeSessions, 1)

	err = manager.Manage(Request, firstAddress, packet.Payload{})
	assert.Nil(t, err)
	assert.Len(t, manager.activeSessions, 1)

	challenge := manager.activeSessions[firstAddress].challenge

	err = manager.Manage(ChallengeResponse, firstAddress, packet.Payload{
		Action: ChallengeResponse,
		Data:   []byte(challenge),
	})
	assert.Nil(t, err)
	assert.Len(t, manager.activeSessions, 1)

	err = manager.Manage(Request, secondAddress, packet.Payload{})
	assert.Nil(t, err)
	assert.Len(t, manager.activeSessions, 1)

	challenge = manager.activeSessions[secondAddress].challenge

	err = manager.Manage(ChallengeResponse, secondAddress, packet.Payload{
		Action: ChallengeResponse,
		Data:   []byte(challenge),
	})
	assert.ErrorIs(t, err, ErrServerFull)

	manager.activeSessions[firstAddress] = Session{
		address:    firstAddress,
		state:      Disconnected,
		lastUpdate: time.Time{},
	}

	err = manager.Manage(Request, firstAddress, packet.Payload{})
	assert.Nil(t, err)
}*/

type mockPacketChannel struct{}

func (m *mockPacketChannel) Listen() (packet.Packet, error) {
	return packet.Packet{}, nil
}

func (m *mockPacketChannel) Send(payload packet.Payload, destination packet.Address) error {
	return nil
}
