package session

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestChallengeBuilder_Build(t *testing.T) {
	length := 10
	builder := challengeBuilder{
		length: length,
		reader: &mockReader{length: length},
	}

	challenge, err := builder.Build()
	assert.Nil(t, err)
	assert.Equal(t, "AAECAwQFBgcICQ==", challenge)
}

func Test_generateRandomBytes(t *testing.T) {
	length := 10
	builder := challengeBuilder{
		length: length,
		reader: &mockReader{length: length},
	}
	expected := []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}

	bytes, err := builder.generateRandomBytes(10)
	assert.Nil(t, err)
	assert.Equal(t, expected, bytes)
}

type mockReader struct {
	length int
}

func (r *mockReader) Read(p []byte) (n int, err error) {
	for i := 0; i < r.length; i++ {
		p[i] = byte(i)
	}

	return r.length, nil
}
