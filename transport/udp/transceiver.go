package udp

import (
	"fmt"
	"log"
	"net"

	"github.com/juju/errors"
)

type ServerTransceiver struct {
	logger *log.Logger

	udpConn net.PacketConn
}

func NewTransceiver(logger *log.Logger, port int) (*ServerTransceiver, error) {
	udpConn, err := net.ListenPacket("udp", fmt.Sprintf(":%d", port))
	if err != nil {
		return nil, errors.Trace(err)
	}

	return &ServerTransceiver{
		logger:  logger,
		udpConn: udpConn,
	}, nil
}

func (t *ServerTransceiver) Listen() ([]byte, net.Addr, error) {
	buf := make([]byte, 1024)

	n, clientAddr, err := t.udpConn.ReadFrom(buf)
	if err != nil {
		return nil, nil, errors.Trace(err)
	}

	if n == 0 {
		return nil, nil, nil
	}

	return buf, clientAddr, nil
}

func (t *ServerTransceiver) Send(payload []byte, host string, port int) error {
	udpAdd, err := net.ResolveUDPAddr("udp", fmt.Sprintf("%s:%d", host, port))
	if err != nil {
		return errors.Trace(err)
	}

	_, err = t.udpConn.WriteTo(payload, udpAdd)
	return err
}

func (t *ServerTransceiver) Close() {
	_ = t.udpConn.Close()
}
