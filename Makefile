PROJECT_NAME := multiplayer-network
FOLDER_BIN = $(CURDIR)/bin
GO_TEST=go test ./...

build:
	go build -o $(FOLDER_BIN)/$(PROJECT_NAME) $(CURDIR)/cmd/repl/main.go

test:
	$(GO_TEST) -coverprofile cover.out

test-race:
	$(GO_TEST) -race

mod:
	go mod tidy
