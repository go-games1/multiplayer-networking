package multiplayer_network

import (
	"log"
	"time"

	"github.com/juju/errors"
	"gitlab.com/go-games1/multiplayer-networking/transport/packet"
	"gitlab.com/go-games1/multiplayer-networking/transport/session"
	"gitlab.com/go-games1/multiplayer-networking/transport/udp"
)

type OnReceive func(p packet.Packet) error

type MultiplayerServer struct {
	logger *log.Logger

	packetChannel  *packet.Channel
	sessionManager session.Manager

	onReceive OnReceive
}

func NewMultiplayerServer(
	logger *log.Logger,
	port int,
	maxConnections int,
	inactivityDuration time.Duration,
	onReceive OnReceive,
) (*MultiplayerServer, error) {
	tx, err := udp.NewTransceiver(logger, port)
	if err != nil {
		return nil, errors.Trace(err)
	}

	packetChannel := packet.NewChannel(tx)

	sessionManager := session.NewInMemoryManager(logger, packetChannel, maxConnections, inactivityDuration)

	return &MultiplayerServer{
		logger: logger,

		packetChannel:  packetChannel,
		sessionManager: sessionManager,

		onReceive: onReceive,
	}, nil
}

func (s *MultiplayerServer) Send(id string, payload packet.Payload) {

}

func (s *MultiplayerServer) Listen() error {
	for {
		p, err := s.packetChannel.Listen()
		if err != nil {
			return errors.Trace(err)
		}

		if p.IsEmpty() {
			continue
		}

		go func() {
			if err := s.handlePacket(p); err != nil {
				s.logger.Println(errors.Trace(err))
			}
		}()
	}
}

func (s *MultiplayerServer) handlePacket(p packet.Packet) error {
	if _, ok := p.Payload.Action.(session.Action); ok {
		err := s.sessionManager.Manage(p.Payload.Action.(session.Action), p.SourceAddress, p.Payload)

		return errors.Trace(err)
	}

	if err := s.onReceive(p); err != nil {
		return errors.Trace(err)
	}

	return nil
}
